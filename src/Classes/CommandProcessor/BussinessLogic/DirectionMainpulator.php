<?php

namespace Classes\CommandProcessor\BussinessLogic;

use Classes\CommandMainpulator\CommandSplit;
use Classes\CommandProcessor\BussinessLogic\ProcessInterface;

/**
 * Class FireDirection that is responsible of fire direction
 * @package Classes\CommandProcessor
 */
class DirectionMainpulator implements ProcessInterface
{
    /**
     * @param string $command
     * @return string of x , y, direction in finalResult
     */
    public function startProcess(String $command): String
    {
        $finalResult ='';
        $directionFatory = new DirectionFactory();
        $commandSplit = new CommandSplit();
        $commandSplitted = $commandSplit->splitCommand($command);
        foreach ($commandSplitted[0] as $pieces) {
            if ($pieces == 'R') {
                $directionFatory->angle == 270 ? $directionFatory->angle = 0 : $directionFatory->angle += 90;
            } elseif ($pieces == 'L') {
                $directionFatory->angle == -270 ? $directionFatory->angle = 0 : $directionFatory->angle -= 90;
            }
            $directionFatory->getDirection($pieces);
        }
        return $finalResult=$directionFatory->xCor.','.$directionFatory->yCor.','.$directionFatory->direction;
    }
}
