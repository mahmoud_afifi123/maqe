<?php

namespace Classes\CommandProcessor\BussinessLogic;

use Classes\Coordinations\XCoordination;
use Classes\Coordinations\YCoordination;
use Classes\Directions\East;
use Classes\Directions\North;
use Classes\Directions\South;
use Classes\Directions\West;

/**
 * Class Factory taht is responsible for mantain directions and coordination
 * @package Classes\CommandProcessor
 */
class DirectionFactory
{
    public $angle = 0;
    public $xCor = 0;
    public $yCor = 0;
    public $direction;

    /**
     * @param $steps that is positive number of steps MAQE should move
     */
    public function getDirection($steps)
    {
        if ($this->angle == 90 | $this->angle == -270) {
            $xCoordinate = new XCoordination();
            $east = new East();
            if (!in_array($steps, array('R', 'L', 'W'))) {
                $this->xCor = $east->updateCoordinate($xCoordinate, (int)$steps);
            }
            $this->direction = $east->getName();
        } elseif ($this->angle == 270 | $this->angle == -90) {
            $xCoordinate = new XCoordination();
            $west = new West();
            if (!in_array($steps, array('R', 'L', 'W'))) {
                $this->xCor = $west->updateCoordinate($xCoordinate, (int)$steps);
            }
            $this->direction = $west->getName();
        } elseif ($this->angle == 180 | $this->angle == -180) {
            $yCoordinate = new YCoordination();
            $south = new South();
            if (!in_array($steps, array('R', 'L', 'W'))) {
                $this->yCor = $south->updateCoordinate($yCoordinate, (int)$steps);
            }
            $this->direction = $south->getName();
        } elseif ($this->angle == 0) {
            $yCoordinate = new YCoordination();
            $north = new North();
            if (!in_array($steps, array('R', 'L', 'W'))) {
                $this->yCor = $north->updateCoordinate($yCoordinate, (int)$steps);
            }
            $this->direction = $north->getName();
        }
    }
}
