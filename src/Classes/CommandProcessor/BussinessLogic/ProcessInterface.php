<?php

namespace Classes\CommandProcessor\BussinessLogic;

/**
 * interface ProcessInterface that process the command calculation and pre operations
 * @package Abstracts
 */
interface ProcessInterface
{
    /**
     * @param string $command supplied
     * @return mixed
     */
    public function startProcess(string $command) : string;
}
