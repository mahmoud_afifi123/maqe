<?php

namespace Classes\CommandProcessor\BussinessLogic;

use Classes\CommandProcessor\BussinessLogic\ProcessInterface;

use Classes\CommandMainpulator\CommandSplit;

/**
 * Class Validation that responsible for validation
 * @package Classes\CommandMainpulator
 */
class Validation implements ProcessInterface
{
    private $errorsArray = null;
    private $allowed = array('R', 'L', 'W');
    private $commandArray;

    /**
     * @param string $command supplied
     * @return string of errors if found
     */
    public function startProcess(string $command): string
    {
        $commandSplit = new CommandSplit();
        $this->commandArray = $commandSplit->splitCommand($command)[0];
        foreach ($this->commandArray as $key => $item) {
            $this->checkCharacters($item);
            $this->checkMovements($key, $item);
            $this->checkEndAndStart();
        }
        return $this->errorsArray ?? '';
    }

    /**
     * @param $item that is piece of command
     */
    public function checkCharacters($item)
    {
        if (strtoupper($item) !== $item) {
            $this->errorsArray .= ",The character '$item' must not be in lower case";
        }
        if (!in_array($item, $this->allowed) and (!preg_match('/[0-9]+/', $item))) {
            $this->errorsArray .= ",The character '$item' not allowed";
        }
    }

    /**
     * @param $key
     * @param $item that is piece of command
     */
    public function checkMovements($key, $item)
    {
        if ($item == '-') {
            $this->errorsArray .= ",There is negative number which not allowed";
        }
        if ($item == 'W' and $this->checkAfter($key) and (!$this->numberAfterW($key) | $this->zeroAfterW($key))) {
            $this->errorsArray .= ",Not allowed to make robot move forward with no number of steps '$item' not allowed";
        }
        if (preg_match('/[0-9]+/', $item) and $this->checkBefore($key) and $this->wBeforeNumber($key)) {
            $this->errorsArray .= ",Not allowed to make robot move without 'W' command";
        }
    }

    /**
     * @param int $key
     * @return bool
     */
    public function checkAfter(int $key): bool
    {
        return isset($this->commandArray[$key + 1]);
    }

    /**
     * @param int $key
     * @return bool
     */
    public function numberAfterW(int $key): bool
    {
        return preg_match('/[0-9]+/', $this->commandArray[$key + 1]);
    }

    /**
     * @param int $key
     * @return bool
     */
    public function zeroAfterW(int $key): bool
    {
        return $this->commandArray[$key + 1] == '0';
    }

    /**
     * @param int $key
     * @return bool
     */
    public function checkBefore(int $key): bool
    {
        return isset($this->commandArray[$key - 1]);
    }

    /**
     * @param int $key
     * @return bool
     */
    public function wBeforeNumber(int $key): bool
    {
        return $this->commandArray[$key - 1] != 'W';
    }

    public function checkEndAndStart()
    {
        if ($this->commandArray[count($this->commandArray) - 1] == 'W') {
            $this->errorsArray .= ",The end of command can not be 'W' character";
        }
        if (preg_match('/[0-9]+/', $this->commandArray[0])) {
            $this->errorsArray .= ",The start of command can not be number";
        }
    }
}
