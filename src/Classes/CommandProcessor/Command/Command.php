<?php

namespace Classes\CommandProcessor\Command;

use Classes\CommandProcessor\BussinessLogic\ProcessInterface;

use Classes\CommandProcessor\Command\CommandInterface;

/**
 * Class Command that is responsible of execute command
 * @package Classes\CommandProcessor
 */
class Command implements CommandInterface
{
    /**
     * @param ProcessInterface $process
     * @param string $command
     * @return mixed
     */
    public static function processTask(ProcessInterface $process, string $command) :string
    {
        return $process->startProcess($command);
    }
}
