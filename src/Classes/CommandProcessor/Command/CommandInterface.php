<?php

namespace Classes\CommandProcessor\Command;

use Classes\CommandProcessor\BussinessLogic\ProcessInterface;

/**
 * Interface CommandInterface
 * @package Classes\CommandProcessor
 */
interface CommandInterface
{
    /**
     * @param ProcessInterface $process ..implements later
     * @param string $command
     * @return mixed
     */
    public static function processTask(ProcessInterface $process, string $command);
}
