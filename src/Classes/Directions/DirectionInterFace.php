<?php

namespace Classes\Directions;

use Classes\Coordinations\CoordinationInterface;

/**
 * Interface DirectionInterFace
 * @package Classes\Directions
 */
interface DirectionInterFace
{
    /**
     * @param CoordinationInterface $coordination
     * @param int $value
     * @return mixed
     */
    public function updateCoordinate(CoordinationInterface  $coordination, int $value);
}
