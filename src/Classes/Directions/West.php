<?php

namespace Classes\Directions;

use Classes\Coordinations\CoordinationInterface;
use Classes\Directions\DirectionInterFace;

/**
 * Class West that is west direction entity
 * @package Classes\Directions
 */
class West implements DirectionInterFace
{

    /**
     * @param CoordinationInterface $coordination
     * @param int $value
     * @return int of coordinate value
     */
    public function updateCoordinate(CoordinationInterface $coordination, int $value): int
    {
        $coordination->increaseCoordinate(-$value);
        return $coordination->getCoordinate();
    }

    /**
     * @return string of direction name
     */
    public function getName(): string
    {
        return 'West';
    }
}
