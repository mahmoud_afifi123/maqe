<?php

namespace Classes\Coordinations;

use Classes\Coordinations\CoordinationInterface;

/**
 * Class XCoordination that is entity of xCoordination
 * @package Classes\Coordinations
 */
class XCoordination implements CoordinationInterface
{
    private static $xCor;

    /**
     * @return int of xcor
     */
    public function getCoordinate(): int
    {
        return static::$xCor;
    }

    public function setCordinateForTest()
    {
        static::$xCor = 5;
    }

    public function setZeroForTest()
    {
        static::$xCor = 0;
    }

    /**
     * @param int $value
     * @return mixed|void
     */
    public function increaseCoordinate(int $value)
    {
        static::$xCor += (int)$value;
    }
}
