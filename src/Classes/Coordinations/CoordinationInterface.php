<?php

namespace Classes\Coordinations;

/**
 * Interface CoordinationInterface
 * @package Classes\Coordinations
 */
interface CoordinationInterface
{
    /**
     * @return int
     */
    public function getCoordinate(): int;

    /**
     * @param int $value
     * @return mixed
     */
    public function increaseCoordinate(int $value);
}
