<?php

namespace Classes\Coordinations;

use Classes\Coordinations\CoordinationInterface;

/**
 * Class YCoordination that is entity of yCoordination
 * @package Classes\Coordinations
 */
class YCoordination implements CoordinationInterface
{
    private static $yCor;

    /**
     * @return int of ycor
     */
    public function getCoordinate(): int
    {
        return static::$yCor;
    }

    public function setCordinateForTest()
    {
        static::$yCor = 5;
    }

    public function setZeroForTest()
    {
        static::$yCor = 0;
    }

    /**
     * @param int $value
     * @return mixed|void
     */
    public function increaseCoordinate(int $value)
    {
        static::$yCor += $value;
    }
}
