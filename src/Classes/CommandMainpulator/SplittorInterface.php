<?php

namespace Classes\CommandMainpulator;

/**
 * Interface SplittorInterface
 * @package Classes\CommandMainpulator
 */
interface SplittorInterface
{
    /**
     * @param string $command
     * @return mixed
     */
    public function splitCommand(string $command);
}
