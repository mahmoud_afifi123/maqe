<?php

namespace Classes\CommandMainpulator;

/**
 * Class CommandSplit that responsible for split command
 * @package Classes\CommandMainpulator
 */
class CommandSplit
{
    /**
     * @param string $command
     * @return array of matched $command regex
     */
    public function splitCommand(string $command): array
    {
        preg_match_all("/[0-9]+|[A-Z]/", $command, $match);
        return $match;
    }
}
