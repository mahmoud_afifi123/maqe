<?php

namespace Controllers;

use Classes\CommandProcessor\BussinessLogic\DirectionMainpulator;
use Controllers\DisplayClass;
use Classes\CommandProcessor\BussinessLogic\Validation;
use Classes\CommandProcessor\Command\Command;

/**
 * Class HomeController that run the client
 * @package Controllers
 */
class HomeController
{

    /**
     * HomeController constructor.
     * @param $command
     */
    public function __construct($command)
    {
        $commandObj = new Command();
        $dispalyer = new DisplayClass();
        $validationOutput = $commandObj->processTask(new Validation(), $command);
        if ($validationOutput == '') {
            $calculationOutput = $commandObj->processTask(new DirectionMainpulator(), $command);
            $dispalyer->displaySuccessMessage($calculationOutput);
        } else {
            $dispalyer->displayErrorsMessage($validationOutput);
        }
    }
}
