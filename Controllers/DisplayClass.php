<?php

namespace Controllers;

/**
 * Class DisplayClass that display the error and sucees
 * @package Controllers
 */
class DisplayClass
{
    /**
     * @param string $successData of the x ,y ,direction
     */
    public function displaySuccessMessage(string $successData)
    {
        $successDataReturned = explode(',', $successData);
        echo "<h1>X: $successDataReturned[0] Y: $successDataReturned[1] Direction: $successDataReturned[2]</h1>";
    }

    /**
     * @param string of error data
     */
    public function displayErrorsMessage(string $errorsData)
    {
        $errorsReturned = explode(',', $errorsData);
        unset($errorsReturned[0]);
        if (!empty($errorsReturned)) {
            echo "<h1>ERRORS</h1><br>";
            foreach ($errorsReturned as $value) {
                echo "-<b>$value</b><br>";
            }
        }
    }
}
