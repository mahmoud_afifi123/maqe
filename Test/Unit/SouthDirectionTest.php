<?php
/**
 * Created by PhpStorm.
 * User: mahmoud.gamal
 * Date: 10/24/2017
 * Time: 7:59 PM
 */

namespace Test\Unit;

use PHPUnit\Framework\TestCase;

class SouthDirectionTest extends TestCase
{
    public function testSouthDirectionwith0Return0()
    {
        $south = new South();
        $ycor = new YCoordination();
        $ycor->setCordinateForTest();
        $this->assertEquals(2, $south->updateCoordinate(new YCoordination(), 3));
    }
}
