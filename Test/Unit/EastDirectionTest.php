<?php
/**
 * Created by PhpStorm.
 * User: mahmoud.gamal
 * Date: 10/24/2017
 * Time: 7:57 PM
 */

namespace Test\Unit;

use PHPUnit\Framework\TestCase;

class EastDirectionTest extends TestCase
{
    public function testEastDirectionwith0Return0()
    {
        $east = new East();
        $xcor = new XCoordination();
        $xcor->setCordinateForTest();
        $this->assertEquals(7, $east->updateCoordinate(new XCoordination(), 2));
    }
}
