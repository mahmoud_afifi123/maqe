<?php
/**
 * Created by PhpStorm.
 * User: mahmoud.gamal
 * Date: 10/24/2017
 * Time: 8:00 PM
 */

namespace Test\Unit;

use PHPUnit\Framework\TestCase;

class NorthDirectionTest extends TestCase
{
    public function testNorthDirectionwith0Return0()
    {
        $north = new North();
        $ycor = new YCoordination();
        $ycor->setCordinateForTest();
        $this->assertEquals(8, $north->updateCoordinate(new YCoordination(), 3));
    }
}
