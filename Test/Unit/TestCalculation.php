<?php

namespace Test\Unit;

use Classes\CommandProcessor\BussinessLogic\DirectionMainpulator;
use Classes\CommandProcessor\Command\Command;
use Classes\Coordinations\CoordinationInterface;
use Classes\Coordinations\XCoordination;
use Classes\Coordinations\YCoordination;
use Classes\Directions\East;
use Classes\Directions\North;
use Classes\Directions\South;
use Classes\Directions\West;
use Interfaces\Coordination;
use PHPUnit\Framework\TestCase;

/**
 * Class TestCalculation
 * @package Test\Unit
 */
class TestCalculation extends TestCase
{
    public $command;

    public function testFireWithW11RReturn11And0AndEast()
    {
        $fireDirection = new DirectionMainpulator();
        $this->assertEquals('4,3,North', $fireDirection->startProcess('W5RW5RW2RW1R'));
    }

    public function testCommandStartProcessWithW11RReturn11And0AndEast()
    {
        $ycor = new YCoordination();
        $ycor->setZeroForTest();
        $xcor = new XCoordination();
        $xcor->setZeroForTest();
        $command = new Command();
        $this->assertEquals('4,3,North', $command->processTask(new DirectionMainpulator(), 'W5RW5RW2RW1R'));
    }

}
