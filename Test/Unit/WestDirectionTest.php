<?php
/**
 * Created by PhpStorm.
 * User: mahmoud.gamal
 * Date: 10/24/2017
 * Time: 7:58 PM
 */

namespace Test\Unit;

use PHPUnit\Framework\TestCase;

class WestDirectionTest extends TestCase
{
    public function testWestDirectionwith0Return0()
    {
        $west = new West();
        $xcor = new XCoordination();
        $xcor->setCordinateForTest();
        $this->assertEquals(3, $west->updateCoordinate(new XCoordination(), 2));
    }
}
